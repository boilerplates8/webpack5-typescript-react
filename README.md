## Built using

- node 16.11.0

- npm  8.0.0

## Install dependencies

- npm install

## Run dev server

- npm start

## Build

- npm build

## Run storybook

- npm run storybook

## Build storybook

- npm run build-storybook

## Testing

- npm run test-jest

## Code style and rules

- npm run lint
- npm run format

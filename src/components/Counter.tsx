import React, { FunctionComponent, useState } from 'react';

import styles from './Counter.module.scss';

export interface CounterProps {
	withImage?: boolean;
}

export const Counter: FunctionComponent<CounterProps> = (props) => {
	const { withImage } = props;
	const [count, setCount] = useState<number>(0);

	return (
		<div className={withImage && styles.formWrapper}>
			<h3 className={styles.title}>
				Update the count and edit src/App.tsx, state is preserved
			</h3>
			<button onClick={() => setCount((c) => c + 1)}>
				Count - {count}
			</button>
		</div>
	);
};

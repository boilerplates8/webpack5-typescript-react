import { configure, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { Counter } from './Counter';

configure({ adapter: new Adapter() });

describe('Counter', () => {
	test('Should render the component', () => {
		// Mock
		const wrapper = shallow(<Counter withImage />);

		// Assertion
		expect(wrapper.find('.title').length).toBe(1);
	});
});

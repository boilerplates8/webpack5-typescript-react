import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { Counter } from './Counter';

export default {
	title: 'Widget/Counter',
	component: Counter,
} as ComponentMeta<typeof Counter>;

const Template: ComponentStory<typeof Counter> = (args) => (
	<Counter {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
	withImage: true,
};

export const Secondary = Template.bind({});
Secondary.args = {
	withImage: false,
};

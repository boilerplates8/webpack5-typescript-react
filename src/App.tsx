import React, { FunctionComponent, Fragment } from 'react';

import { Counter } from './components/Counter';
import './styles/main.scss';

export const App: FunctionComponent = () => {
	return (
		<Fragment>
			<h1>React TypeScript Webpack Starter Template</h1>
			<Counter />
		</Fragment>
	);
};
